//
//  ViewController.swift
//  CustomAnnotation
//
//  Created by Salvador Lopez on 21/06/23.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mapView.delegate = self
        mapView.register(MyCustomAnnotation.self, forAnnotationViewWithReuseIdentifier: "myAnnotation")
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(setAnnotation(_ :)))
        mapView.addGestureRecognizer(tapGesture)
    }

    @objc func setAnnotation(_ gesture: UITapGestureRecognizer){
        // Get coordinates from map
        let touchCoordinate = gesture.location(in: mapView)
        let coordinate = mapView.convert(touchCoordinate, toCoordinateFrom: mapView)
        
        // Create annotation
        let annotation = MKPointAnnotation()
        annotation.coordinate = coordinate
        
        // Add annotation to Map
        mapView.addAnnotation(annotation)
    }

}

extension ViewController: MKMapViewDelegate{
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        //Personalizar el annotation
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        return mapView.dequeueReusableAnnotationView(withIdentifier: "myAnnotation", for: annotation)
    }
    
}

