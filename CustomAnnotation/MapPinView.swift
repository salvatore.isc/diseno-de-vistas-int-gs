//
//  MapPinView.swift
//  CoreLocationSb
//
//  Created by Salvador Lopez on 21/06/23.
//

import UIKit

class MapPinView: UIView {
    
    // Codigo
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupImageView()
    }
    
    // SB
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupImageView()
    }
    
    
    func setupImageView(){
        let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 32, height: 32))
        imgView.image = UIImage(named: "pin-g0a4e84ab4_640")
        addSubview(imgView)
    }

    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
