//
//  MyCustomAnnotation.swift
//  CoreLocationSb
//
//  Created by Salvador Lopez on 21/06/23.
//

import UIKit
import MapKit

class MyCustomAnnotation: MKAnnotationView {

    override init(annotation: MKAnnotation?, reuseIdentifier: String?){
        
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        frame = CGRect(x: 0, y: 0, width: 32, height: 32)
        centerOffset =  CGPoint(x: 0, y: -frame.size.height / 2)
        canShowCallout = true
        setupUI()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("This way is not implemented yet")
    }
    
    func setupUI(){
        backgroundColor = .clear
        let view = MapPinView()
        addSubview(view)
        view.frame = bounds
    }

}
