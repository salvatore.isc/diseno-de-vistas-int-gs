//
//  MyTableViewCell.swift
//  CoreDataSb
//
//  Created by Salvador Lopez on 22/06/23.
//

import UIKit

class MyTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLb: UILabel!
    @IBOutlet weak var emailLb: UILabel!
    @IBOutlet weak var imgCell: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
