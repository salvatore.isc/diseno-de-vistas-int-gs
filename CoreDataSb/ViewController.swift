//
//  ViewController.swift
//  CoreDataSb
//
//  Created by Salvador Lopez on 22/06/23.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var userNameTextField: UITextField!
    
    var users = [User]()
    var idSelected: UUID?
    
    @IBAction func saveUser(_ sender: Any) {
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        
        //Nueva Entity
        let userEntity = NSEntityDescription.entity(forEntityName: "User", in: manageContext)
        
        //Crear registro
        let user = NSManagedObject(entity: userEntity!, insertInto: manageContext)
        user.setValue(UUID(), forKey: "id")
        user.setValue(userNameTextField.text, forKey: "username")
        user.setValue(emailTextField.text, forKey: "email")
        user.setValue(passwordTextField.text, forKey: "password")
        
        //Guardar Context
        do{
            try manageContext.save()
            print("Guardado con exito...")
            loadUsers()
        }catch{
            print("Error: \(error)")
        }
        
    }
    
    @IBAction func updateUser(_ sender: Any) {
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        
        // Request + Predicate
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        fetchRequest.predicate = NSPredicate(format: "id == %@", idSelected! as CVarArg)
        
        // Fetch
        do{
            let records = try manageContext.fetch(fetchRequest)
            
            if let firstResult = records.first as? NSManagedObject{
                firstResult.setValue(self.userNameTextField.text, forKey: "username")
                firstResult.setValue(self.emailTextField.text, forKey: "email")
                firstResult.setValue(self.passwordTextField.text, forKey: "password")
                
                //Save
                do{
                    try manageContext.save()
                    print("Se aplicaros los cambios")
                    loadUsers()
                }catch{
                    print("Error: \(error)")
                }
                
            }
            
        }catch{
            print("Error: \(error)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
        loadUsers()
    }
    
    func loadUsers(){
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        
        //Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        
        //Fetch
        do{
            let result = try manageContext.fetch(fetchRequest)
            if result.isEmpty{
                print("No hay resgistros")
            }else{
                //Limpiar
                self.users.removeAll()
                result.forEach { user in
                    if let user = user as? User {
                        self.users.append(user)
                    }
                }
                cleanTextFields()
                tableView.reloadData()
            }
        }catch{
            
        }
    }
    
    func deleteUser(_ userToDeleteId: UUID){
        // Instancia AppDelegate
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        //Obtener Contexto
        let manageContext = appDelegate.persistentContainer.viewContext
        
        //Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        fetchRequest.predicate = NSPredicate(format: "id == %@", userToDeleteId as CVarArg)
        
        do{
            let records = try manageContext.fetch(fetchRequest)
            if let firstRecord = records.first as? NSManagedObject{
                manageContext.delete(firstRecord)
                try manageContext.save()
                print("Eliminado...")
                loadUsers()
            }
        }catch{
            print("Error: \(error)")
        }
    }
    
    func cleanTextFields(){
        self.userNameTextField.text = ""
        self.emailTextField.text = ""
        self.passwordTextField.text = ""
    }


}

extension ViewController: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyTableViewCell", for: indexPath) as! MyTableViewCell
        cell.usernameLb.text = users[indexPath.row].username
        cell.emailLb.text = users[indexPath.row].email
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return "Core Data (Users)"
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.userNameTextField.text = users[indexPath.row].username
        self.emailTextField.text = users[indexPath.row].email
        self.passwordTextField.text = users[indexPath.row].password
        self.idSelected = users[indexPath.row].id
    }
    
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteUser(users[indexPath.row].id!)
        }
    }
    
    
}
