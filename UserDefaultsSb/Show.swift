//
//  Show.swift
//  UserDefaultsSb
//
//  Created by Salvador Lopez on 22/06/23.
//

import Foundation

class Show: Codable {
    var id: Int
    var name : String
    var time : String
    
    init(id:Int ,name: String, time:String){
        self.id = id
        self.name = name
        self.time = time
    }
}
