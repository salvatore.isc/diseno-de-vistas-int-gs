//
//  ViewController.swift
//  UserDefaultsSb
//
//  Created by Salvador Lopez on 22/06/23.
//

import UIKit

class ViewController: UIViewController {
    
    var slider = UISlider()
    var label = UILabel()
    let wScreen = UIScreen.main.bounds.width
    let hScreen = UIScreen.main.bounds.height
    
    var preferredVolume: Int = 0 {
        didSet {
            updateUI()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
        preferredVolume = UserDefaults.standard.integer(forKey: Constantes.preferredVolume)
        
        //MARK: SAEVING DATA IN USERDEFAULTS
        //let showTv = Show(id: 10, name: "The Simpsons", time: "8:15pm")
        /*do{
            let data = try JSONEncoder().encode(showTv)
            UserDefaults.standard.set(data, forKey: "ShowTv")
            print("Save it!")
        }catch {
            print("Error: \(error)")
        }*/
        
        //MARK: GETTING DATA IN USERDEFAULTS
        let data = UserDefaults.standard.data(forKey: "ShowTv")!
        print(data)
        do{
            let showTv = try JSONDecoder().decode(Show.self, from: data)
            dump(showTv)
        }catch{
            print("Error: \(error)")
        }
        
    }
    
    func updateUI(){
        label.text = "\(preferredVolume)"
        slider.value = Float(preferredVolume)
    }
    
    func setupUI(){
        //MARK: UILabel
        label.frame = CGRect(x: 15, y: 50, width: 120, height: 30)
        label.textColor = .blue
        self.view.addSubview(label)
        
        //MARK: UISlider
        slider.frame = CGRect(x: 15, y: 100, width: wScreen/2, height: 15)
        slider.minimumValue = 0
        slider.maximumValue = 10
        slider.addTarget(self, action: #selector(updateSlider(sender:)), for: .valueChanged)
        self.view.addSubview(slider)
        
        //MARK: UIButton
        let saveBtn = UIButton(frame: CGRect(x: 15, y: 150, width: 90, height: 30))
        saveBtn.setTitle("Save", for: .normal)
        saveBtn.backgroundColor = .blue
        saveBtn.addTarget(self, action: #selector(saveConfiguration), for: .touchUpInside)
        self.view.addSubview(saveBtn)
    }
    
    @objc func updateSlider(sender: UISlider){
        preferredVolume = Int(sender.value)
    }
    
    @objc func saveConfiguration(){
        UserDefaults.standard.set(preferredVolume, forKey: Constantes.preferredVolume)
        print("Save!")
    }


}

