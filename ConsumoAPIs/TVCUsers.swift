//
//  TVCUsers.swift
//  ConsumoAPIs
//
//  Created by Salvador Lopez on 20/06/23.
//

import UIKit

class TVCUsers: UITableViewController {
    
    var urlService = "https://jsonplaceholder.typicode.com/users"
    var users = [User]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        var request = URLRequest(url: URL(string: self.urlService)!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) {
            data, response, error in
            if error != nil{
                print("Error: \(error)")
                //TODO: RESPONSE
            }else {
                do{
                    let arrUser = try JSONDecoder().decode([User].self, from: data!)
                    //dump(arrUser)
                    self.users = arrUser
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }catch{
                    print("Error: \(error)")
                }
            }
            
        }.resume()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return users.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: "cellEmployee", for: indexPath)
        cell.textLabel?.text = users[indexPath.row].name
        cell.detailTextLabel?.text = users[indexPath.row].email
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        dump(users[indexPath.row])
    }

}
