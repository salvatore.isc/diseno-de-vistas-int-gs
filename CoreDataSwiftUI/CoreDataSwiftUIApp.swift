//
//  CoreDataSwiftUIApp.swift
//  CoreDataSwiftUI
//
//  Created by Salvador Lopez on 23/06/23.
//

import SwiftUI

@main
struct CoreDataSwiftUIApp: App {
    let persistentController = PersistenceController.shared
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistentController.container.viewContext)
        }
    }
}
