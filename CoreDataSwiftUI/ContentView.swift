//
//  ContentView.swift
//  CoreDataSwiftUI
//
//  Created by Salvador Lopez on 23/06/23.
//

import SwiftUI
import CoreData

struct ContentView: View {
    
    @Environment(\.managedObjectContext) private var viewContext
    @FetchRequest(sortDescriptors: [NSSortDescriptor(keyPath:\User.username, ascending: true)])
    
    private var users: FetchedResults<User>
    
    @State var id: UUID!
    @State var usernameText: String = ""
    @State var emailText: String = ""
    @State var passwordText: String = ""
    
    var body: some View {
        VStack {
            HStack{
                VStack{
                    Text("Username")
                        .frame(width: 100, height: 35,alignment: .leading)
                    Text("Email")
                        .frame(width: 100, height: 35,alignment: .leading)
                    Text("Password")
                        .frame(width: 100, height: 35,alignment: .leading)
                }
                .padding()
                VStack{
                    TextField("Ingresa tu nombre de usuario", text: $usernameText)
                        .textFieldStyle(.roundedBorder)
                    TextField("Ingresa tu email", text: $emailText)
                        .textFieldStyle(.roundedBorder)
                    TextField("Ingresa tu password", text: $passwordText)
                        .textFieldStyle(.roundedBorder)
                }
                .padding()
            }
            .padding()
            HStack{
                Button {
                    print("Saving....")
                    if usernameText != "" && emailText != "" && passwordText != "" {
                        addUser(name: usernameText, email: emailText, pass: passwordText)
                    }
                } label: {
                    Text("Save")
                        .frame(width: 100,height: 34,alignment: .center)
                        .font(.subheadline)
                        .foregroundColor(.white)
                        .background(Color.blue)
                        .cornerRadius(10)
                }
                Button {
                    print("Updating....")
                    if let id = id {
                        updateUser(id: id, user: usernameText, email: emailText, pass: passwordText)
                    }
                } label: {
                    Text("Update")
                        .frame(width: 100,height: 34,alignment: .center)
                        .font(.subheadline)
                        .foregroundColor(.white)
                        .background(Color.blue)
                        .cornerRadius(10)
                }
            }
            .padding()
            NavigationView{
                List{
                    ForEach(users){
                        user in
                        Text(user.username!)
                            .onTapGesture {
                                id = user.id
                                usernameText = user.username!
                                emailText = user.email!
                                passwordText = user.password!
                            }
                    }
                    .onDelete(perform: deleteUsers)
                }
            }
        }
        .padding()
    }
    
    private func addUser(name:String,email:String,pass:String){
        let user = User(context: viewContext)
        
        user.id = UUID()
        user.username = name
        user.email = email
        user.password = pass
        
        do{
            try viewContext.save()
        }catch{
            print("Error saving user: \(user)")
        }
    }
    
    private func updateUser(id:UUID, user:String, email:String, pass: String){
        
        let fetchRequest: NSFetchRequest<User> = User.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id == %@", id as CVarArg)
        
        do{
            let results = try viewContext.fetch(fetchRequest)
            
            if let userResult = results.first {
                userResult.username = user
                userResult.email = email
                userResult.password = pass
                try viewContext.save()
            }
        }catch{
            print("Error updating user: \(error)")
        }
        
    }
    
    private func deleteUsers(offset: IndexSet){
        offset.map { users[$0] }.forEach(viewContext.delete)
        do{
            try viewContext.save()
        }catch{
            print("Error deleting: \(error)")
        }
    }
    
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().environment(\.managedObjectContext, PersistenceController.preview.container.viewContext)
    }
}
