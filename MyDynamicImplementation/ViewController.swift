//
//  ViewController.swift
//  MyDynamicImplementation
//
//  Created by Salvador Lopez on 23/06/23.
//

import UIKit
import MyDynamicFramework

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        MyDynamicFramework.MyDynamicLibrary.sayHello()
    }


}

