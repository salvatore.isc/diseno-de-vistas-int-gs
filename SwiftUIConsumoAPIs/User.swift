//
//  User.swift
//  SwiftUIConsumoAPIs
//
//  Created by Salvador Lopez on 21/06/23.
//

import Foundation

struct User: Decodable, Identifiable {
    let id: Int? //UUID
    let name, username, email: String?
    let address: Address?
    let phone, website: String?
    let company: Company?
}


struct Address: Decodable {
    let street, suite, city, zipcode: String?
    let geo: Geo?
}


struct Geo: Decodable {
    let lat, lng: String?
}


struct Company: Decodable {
    let name, catchPhrase, bs: String?
}
