//
//  SwiftUIConsumoAPIsApp.swift
//  SwiftUIConsumoAPIs
//
//  Created by Salvador Lopez on 21/06/23.
//

import SwiftUI

@main
struct SwiftUIConsumoAPIsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
