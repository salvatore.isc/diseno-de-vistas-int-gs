//
//  DetailUserView.swift
//  SwiftUIConsumoAPIs
//
//  Created by Salvador Lopez on 21/06/23.
//

import SwiftUI

struct DetailUserView: View{
    var user: User
    var body: some View{
        VStack{
            /*ZStack{
                Color.blue
                    .frame(width: 210, height: 210)
                    .cornerRadius(110)
                Image("logo")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 200, height: 200)
                    .background(.yellow)
                    .cornerRadius(100)
            }*/
            VStack{
                Circle()
                    .frame(width: 210, height: 210)
                    .overlay{
                        Image("logo")
                            .resizable()
                            .aspectRatio(contentMode: .fit)
                            .frame(width: 200, height: 200)
                            .background(.yellow)
                            .cornerRadius(100)
                    }
            }
            VStack{
                Text(user.name!)
                    .font(.largeTitle)
                    .fontWeight(.ultraLight)
                    .foregroundColor(.blue)
                Text(user.email!)
                    .font(.body)
                    .fontWeight(.thin)
            }
            VStack{
                TextField("Name", text: .constant(user.name!))
                    .textFieldStyle(.roundedBorder)
                TextField("Username", text: .constant(user.username!))
                    .textFieldStyle(.roundedBorder)
                TextField("Correo", text: .constant(user.email!))
                    .textFieldStyle(.roundedBorder)
                TextField("Telefono", text: .constant(user.phone!))
                    .textFieldStyle(.roundedBorder)
                TextField("Website", text: .constant(user.website!))
                    .textFieldStyle(.roundedBorder)
            }
            .padding()
            Button{
                //TODO: Save this
            } label: {
                Text("Guardar")
                    .frame(width: 100,alignment: .center)
                    .font(.headline)
                    .foregroundColor(.white)
                    .padding()
                    .background(Color.blue)
                    .cornerRadius(50)
            }
        }
    }
}

struct DetailUserView_Previews: PreviewProvider {
    static var previews: some View {
        let user = User(id: 1, name: "Test", username: "TEST", email: "TEST", address: Address(street: "Add", suite: "Add", city: "Add", zipcode: "Add", geo: Geo(lat: "lat", lng: "lon ")), phone: "TEST", website: "TEST", company: Company(name: "comp", catchPhrase: "comp", bs: "bs"))
        DetailUserView(user: user)
    }
}

