//
//  ViewController.swift
//  MapKitDemo
//
//  Created by Salvador Lopez on 21/06/23.
//

import UIKit
import MapKit

class ViewController: UIViewController {
    
    //19.408237, -99.090816 //Source
    //19.435151, -99.140752 //Destination
    
    @IBOutlet weak var mapView: MKMapView!
    
    var latitud: CLLocationDegrees = 19.408237
    var longitud: CLLocationDegrees = -99.090816
    
    var latitudDelta: CLLocationDegrees = 0.010
    var longitudDelta: CLLocationDegrees = 0.010
    
    var latitud2: CLLocationDegrees = 19.435151
    var longitud2: CLLocationDegrees = -99.140752
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        mapView.delegate = self
        initSetRegion()
        initSetAnnotations()
        initPlaceMarks()
    }

    func initSetRegion(){
        let span = MKCoordinateSpan(latitudeDelta: latitudDelta, longitudeDelta: longitudDelta)
        let coordinates = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
        let region = MKCoordinateRegion(center: coordinates, span: span)
        mapView.setRegion(region, animated: true)
    }
    
    func initSetAnnotations(){
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
        annotation.title = "Ciudad Deportiva"
        annotation.subtitle = "Puerta 7 - Foro Sol"
        mapView.addAnnotation(annotation)
        //mapView.selectAnnotation(annotation, animated: true)
        
        let annotation2 = MKPointAnnotation()
        annotation2.coordinate = CLLocationCoordinate2D(latitude: latitud2, longitude: longitud2)
        annotation2.title = "Bellas Artes"
        annotation2.subtitle = "CDMX"
        mapView.addAnnotation(annotation2)
    }
    
    func initPlaceMarks(){
        // Coordenadas
        
        let coordinateOrigin = CLLocationCoordinate2D(latitude: latitud, longitude: longitud)
        let coordinateDestination = CLLocationCoordinate2D(latitude: latitud2, longitude: longitud2)
        
        // Placemark / MapItem
        
        let sourcePlaceMark = MKPlacemark(coordinate: coordinateOrigin)
        let destinationPlaceMark = MKPlacemark(coordinate: coordinateDestination)
        
        let sourceMapItem = MKMapItem(placemark: sourcePlaceMark)
        let destinationMapItem = MKMapItem(placemark: destinationPlaceMark)
        
        // Request
        let direccionsRequest = MKDirections.Request()
        direccionsRequest.source = sourceMapItem
        direccionsRequest.destination = destinationMapItem
        direccionsRequest.transportType = .walking
        
        // Get Direccions
        let direccions = MKDirections(request: direccionsRequest)
        direccions.calculate {
            responseDireccions, error in
            if let error = error {
                print("Error al obtener las direcciones: \(error)")
            }
            if let response = responseDireccions {
                print(response.routes.count)
                if let ruta = response.routes.first {
                    print(ruta.name)
                    print(ruta.distance)
                    print(ruta.hasTolls)
                    print(ruta.expectedTravelTime)
                    self.mapView.addOverlay(ruta.polyline, level: .aboveRoads)
                    self.mapView.setRegion(MKCoordinateRegion(ruta.polyline.boundingMapRect), animated: true)
                    ruta.steps.forEach { paso in
                        print(paso.instructions)
                    }
                }
            }
        }
    }

}

extension ViewController : MKMapViewDelegate{
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        let renderer = MKPolylineRenderer(overlay: overlay)
        renderer.strokeColor = .blue
        renderer.lineWidth = 2
        renderer.lineDashPattern = [2,4]
        renderer.alpha = 0.8
        return renderer
    }
}
