//
//  CoreLocationSwiftUIApp.swift
//  CoreLocationSwiftUI
//
//  Created by Salvador Lopez on 22/06/23.
//

import SwiftUI

@main
struct CoreLocationSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
