//
//  ViewController.swift
//  CoreDataRelationship
//
//  Created by Salvador Lopez on 23/06/23.
//

import UIKit
import CoreData

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        //crearRegsitro()
        obtenerRegistro()
    }
    
    func crearRegsitro(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let manageContext = appDelegate.persistentContainer.viewContext
        
        //Persona Entity
        let personaEntity = NSEntityDescription.entity(forEntityName: "Persona", in: manageContext)
        
        //Crear registro Persona
        let persona = NSManagedObject(entity: personaEntity!, insertInto: manageContext)
        persona.setValue(UUID(), forKey: "id")
        persona.setValue("test", forKey: "nombre")
        persona.setValue("test@test.com", forKey: "correo")
        persona.setValue("555666", forKey: "telefono")
        
        //Persona Ruta
        let rutaEntity = NSEntityDescription.entity(forEntityName: "Ruta", in: manageContext)
        
        //Crear registro Ruta
        let ruta = NSManagedObject(entity: rutaEntity!, insertInto: manageContext)
        ruta.setValue("$200", forKey: "adeudo")
        ruta.setValue("CDMX", forKey: "ciudad")
        ruta.setValue("Av. Desierto de los Leones #1234", forKey: "direccion")
        
        persona.setValue(ruta, forKey: "ruta")
        
        do{
            try manageContext.save()
            print("Guardado con exito...")
        }catch{
            print("Error: \(error)")
        }
        
    }
    
    func obtenerRegistro(){
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let manageContext = appDelegate.persistentContainer.viewContext
        
        // Crear fetch request
        let fetchRequestPersona = NSFetchRequest<NSFetchRequestResult>(entityName: "Persona")
        
        do{
            let personas =  try manageContext.fetch(fetchRequestPersona) as! [NSManagedObject]
            
            //Iteration
            for persona in personas {
                print("--> PERSONA")
                print("id: \(persona.value(forKey:"id") as! UUID)")
                print("Nombre: \(persona.value(forKey:"nombre") as! String)")
                print("Correo: \(persona.value(forKey:"correo") as! String)")
                print("Telefono: \(persona.value(forKey:"telefono") as! String)")
                print("--> RUTA")
                if let ruta = persona.value(forKey: "ruta") as? NSManagedObject {
                    print("Adeudo: \(ruta.value(forKey:"adeudo") as! String)")
                    print("Ciudad: \(ruta.value(forKey:"ciudad") as! String)")
                    print("Direccion: \(ruta.value(forKey:"direccion") as! String)")
                }
            }
            
        }catch{
            print("Error: \(error)")
        }
        
    }


}

