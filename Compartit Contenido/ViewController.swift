//
//  ViewController.swift
//  Compartit Contenido
//
//  Created by Salvador Lopez on 19/06/23.
//

import UIKit

class ViewController: UIViewController {
    
    let img = UIImage(named: "logo")
    @IBOutlet weak var txtToShare: UITextField!
    @IBAction func actShare(_ sender: Any) {
        let activityController = UIActivityViewController(activityItems: [self], applicationActivities: nil)
        present(activityController, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
}

extension ViewController: UIActivityItemSource{
    
    func activityViewControllerPlaceholderItem(_ activityViewController: UIActivityViewController) -> Any {
        return "Este es una especie de placeholder del contenido a compartir..."
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, itemForActivityType activityType: UIActivity.ActivityType?) -> Any? {
        var valueToShare: Any!
        if activityType == .message {
            valueToShare = "Solo me mostrare en la app de #message..."
        }else if activityType == .mail {
            valueToShare = "Solo me muestro en el app de #mail..."
            
        }else if activityType == .copyToPasteboard {
            valueToShare = "Me muestro solo en el portapapeles..."
        }else {
            valueToShare = "Me muestro en los demas activity types..."
        }
        return valueToShare
    }
    
    func activityViewController(_ activityViewController: UIActivityViewController, subjectForActivityType activityType: UIActivity.ActivityType?) -> String {
        return "Aqui va el asunto..."
    }
    
}
