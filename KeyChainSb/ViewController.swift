//
//  ViewController.swift
//  KeyChainSb
//
//  Created by Salvador Lopez on 22/06/23.
//

import UIKit

struct Credentials {
    var username: String
    var password: String
}

enum KeychainError: Error {
    case noPassword // No se encuentre ningun valor asociado a la constraseña
    case unexpectedPasswordData // Los datos de contraseña encontrados en el keychain no son validos
    case unhandleError(status: OSStatus) // Cualquier otro error de keychain. // OSStatus Sistema Operativo
}

class ViewController: UIViewController {
    
    var credenciales = Credentials(username: "salvador@gmail.com", password: "qwerty123")
    let server = "www.example.com"

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        //MARK: ADD
        /*do {
            try addItem(credenciales: credenciales, server: server)
        } catch let error as KeychainError {
            switch error {
            case .noPassword:
                print("No se encuentra ningún valor asociado a la contraseña")
            case .unexpectedPasswordData:
                print("Los datos de contraseña encontrados en Keychain no son válidos")
            case .unhandleError(let status):
                print("Cualquier otro error de Keychain: \(status)")
            }
        } catch {
            print("Error desconocido: \(error.localizedDescription)")
        }*/
        
        //MARK: SEARCH
        do {
            try queryItem(server: server)
        } catch let error as KeychainError {
            switch error {
            case .noPassword:
                print("No se encuentra ningún valor asociado a la contraseña")
            case .unexpectedPasswordData:
                print("Los datos de contraseña encontrados en Keychain no son válidos")
            case .unhandleError(let status):
                print("Cualquier otro error de Keychain: \(status)")
            }
        } catch {
            print("Error desconocido: \(error.localizedDescription)")
        }

        
    }

    //MARK: Adding Item
    func addItem(credenciales: Credentials, server: String) throws {
        dump(credenciales)
        
        let account = credenciales.username
        let password = credenciales.password
            
        let query: [String: Any] = [
            kSecClass as String: kSecClassInternetPassword,
            kSecAttrAccount as String: account,
            kSecAttrServer as String: server,
            kSecValueData as String: password.data(using: .utf8)!,
            kSecReturnAttributes as String: true
        ]
        
        print(query)
        
        let status = SecItemAdd(query as CFDictionary, nil)
        
        guard status == errSecSuccess else {
            throw KeychainError.unhandleError(status: status)
        }
        
        print("Elemento del keychain guardado de manera exitosa")
        
    }
    
    
    //MARK: Search Item
    func queryItem(server: String) throws {
        let querySearch: [String: Any] = [
            kSecClass as String: kSecClassInternetPassword,
            kSecAttrServer as String: server,
            //kSecMatchLimit as String: kSecMatchLimitOne, // limitar el resultado a un solo elemento
            kSecReturnAttributes as String: true, // obtener los datos del elemento del Keychain
            kSecReturnData as String: true // obtener los datos del elemento del Keychain
        ]
        
        // Search
        var item: CFTypeRef?
        let status = SecItemCopyMatching(querySearch as CFDictionary, &item)
        
        guard status != errSecItemNotFound else {
            throw KeychainError.noPassword
        }
        
        guard status == errSecSuccess else {
            throw KeychainError.unhandleError(status: status)
        }
        
        // Validation
        guard let existingItem = item as? [String: Any],
              let passwordData = existingItem[kSecValueData as String] as? Data,
              let password = String(data: passwordData, encoding: .utf8),
              let account = existingItem[kSecAttrAccount as String] as? String else {
            throw KeychainError.unexpectedPasswordData
        }
        
        let attributes = Credentials(username: account, password: password)
        dump(attributes)
        
    }
    
    

}

