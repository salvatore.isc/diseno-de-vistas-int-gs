//
//  VCLogin.swift
//  Diseño de Vistas Int GS
//
//  Created by Salvador Lopez on 19/06/23.
//

import UIKit

class VCLogin: UIViewController {
    
    @IBAction func getBack(_ sender: Any) {
        self.dismiss(animated: true){
            print("Dismiss complete..")
        }
    }
    
    var textColor = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        print("1. viewDidLoad [VCL]")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("2. viewWillAppear [VCL]")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("3. viewDidAppear [VCL]")
        print(textColor)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("4. viewWillDisappear [VCL]")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("5. viewDidDisappear [VCL]")
    }

}
