//
//  VCWebContainer.swift
//  ConsumoAPIs
//
//  Created by Salvador Lopez on 20/06/23.
//

import UIKit
import WebKit

class VCWebContainer: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /*if let url  = URL(string: "http://www.aprendeinglessila.com/2013/06/this-that-these-those/"){
            let request = URLRequest(url: url)
            webView.load(request)
        }*/
        let contenido = """

<div class="no-overflow" id="yui_3_17_2_1_1687273432829_25"><h1 class="title">4.5 UIWebView</h1>
<p>Una clase de <i>UIKit</i>, permite embeber contenido Web en la aplicación.
</p>
<p>Tiene un comportamiento muy similar a un navegador web.
</p>
<p>Está basado en <i>WebKit.</i>
</p>
<ul id="yui_3_17_2_1_1687273432829_24">
    <li id="yui_3_17_2_1_1687273432829_23">Mayor control en el proceso de carga de páginas. </li>
    <li>JavaScript injection.</li>
</ul>
<p>Hace uso de ATS.
</p>
<p>Puede mostrar diferentes tipos de contenido.<br>
</p>
<ul>
    <li>El sitio web a mostrar es apuntado con una URL.</li>
    <li>HTML, JavaScript, PDF´s, etc.</li>
</ul>
<p>Petición asíncrona a un URL:<br>
</p>
<p style="margin-left: 20px;">func loadRequest(_ request: URLRequest)
</p>
<p style="text-align: center;"><br>
</p>
<p style="text-align: center;"><img src="https://www.filepicker.io/api/file/ZvKhC7xAS0OYYJC54PJ8" style="width: 512px; height: 317px;" width="512" height="317">
</p>
<p><strong><br></strong>
</p>
<p><strong>Webkit</strong> es una plataforma<em> opensource</em> que contiene la infraestructura para tener características propias de un navegador web.
</p>
<p>Carga un <em>string</em> conteniendo HTML:
</p>
<p>func loadHTMLString(_ string: String, baseURL: URL?)
</p>
<p>Carga bytes:
</p>
<p>func loadData(_ data: Data, mimeType MIMEType: String, textEncodingName: String, baseURL: URL)
</p>
<p>Ejemplos y resultado final de la conexión al web services:
</p>
<p style="text-align: center;"><img src="https://www.filepicker.io/api/file/6rO2EKmvSpe97KSurITN">
</p></div>

"""
        
        webView.loadHTMLString(contenido, baseURL: nil)
        
    }

}
