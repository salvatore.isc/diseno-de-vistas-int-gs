//
//  ViewController.swift
//  Diseño de Vistas Int GS
//
//  Created by Salvador Lopez on 19/06/23.
//

import UIKit

class ViewController: UIViewController, DataDelegateBack {

    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UITextField!
    
    @IBAction func actForgotPass(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let vcForgotPass = storyboard.instantiateViewController(withIdentifier: "idForgotPass") as? VCForgotPass{
            // Sin Navigation Controller
            self.present(vcForgotPass, animated: true)
            // Navigation Controller
            //self.navigationController?.pushViewController(vcForgotPass, animated: true)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("1. viewDidLoad [VC]")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        print("2. viewWillAppear [VC]")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        print("3. viewDidAppear [VC]")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("4. viewWillDisappear [VC]")
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        print("5. viewDidDisappear [VC]")
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var performsegue = false
        
        if identifier != "segueLogin"{
            performsegue = true
        }else{
            // Evaluar credenciales
            if username.text == "root" && password.text == "toor" {
                performsegue = true
            }
        }
        
        return performsegue
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if let vcd = segue.destination as? VCLogin{
            vcd.view.backgroundColor = UIColor.red
            vcd.textColor = "Rojo"
        }else if let vcr = segue.destination as? VCRegistro{
            vcr.delegate = self
        }
    }
    
    func didReceiveDataBack(_ data: String) {
        print("Recibi valor del VCRegistro: \(data)")
        username.text = data
    }


}

