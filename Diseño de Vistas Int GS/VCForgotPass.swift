//
//  VCForgotPass.swift
//  Diseño de Vistas Int GS
//
//  Created by Salvador Lopez on 19/06/23.
//

import UIKit

class VCForgotPass: UIViewController {

    @IBOutlet weak var containerShareBtn: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //let shareBtn = Bundle.main.loadNibNamed("ShareBtn", owner: self)?.first as? ShareBtn
        //shareBtn?.frame = containerShareBtn.bounds
        //shareBtn?.labelShareBtn.text = "Compartir ;)"
        //containerShareBtn.addSubview(shareBtn!)
        
        let btns = Bundle.main.loadNibNamed("customBtn", owner: self) as? [CustomBtn]
        var position = 10
        if let btns = btns{
            for btn in btns {
                position += 10
                btn.frame = CGRect(x: position, y: position, width: 50, height: 50)
                containerShareBtn.addSubview(btn)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
