//
//  VCRegistro.swift
//  Diseño de Vistas Int GS
//
//  Created by Salvador Lopez on 19/06/23.
//

import UIKit

protocol DataDelegateBack: AnyObject{
    func didReceiveDataBack(_ data: String)
}

class VCRegistro: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var confirmarPassword: UITextField!
    @IBOutlet weak var password: UITextField!
    
    @IBAction func actGetBack(_ sender: Any) {
        if username.text != ""{
            let data = username.text!
            delegate?.didReceiveDataBack(data)
            self.dismiss(animated: true)
        }
    }
    
    @IBAction func actEnviar(_ sender: Any) {
        
    }
    
    var delegate: DataDelegateBack?
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }

}
