//
//  TaskDataStore.swift
//  SQLiteSPM
//
//  Created by Salvador Lopez on 23/06/23.
//

import Foundation
import SQLite

class TaskDataStore{
    
    //Nombre del directorio
    static let DIR_TASK_DB = "taskdb"
    //Nombre archivo BD
    static let STORE_DB_NAME = "task.sqlite3"
    
    //Scheme Type
    private let tasks = Table("Tasks")
    
    //Valores para las columnas de la tabla
    private let id = Expression<Int64>("id")
    private let taskName = Expression<String>("name")
    private let date = Expression<Date>("date")
    private let status = Expression<Bool>("status")
    
    //Singleton
    static let shared = TaskDataStore()
    
    //Declaracion de la conexion a la BD SQLite
    private var db: Connection? = nil
    
    private init(){
        // Representa el directorio de documento del usuario
        if let docDir = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let dirPath = docDir.appendingPathComponent(Self.DIR_TASK_DB)
            do{
                try FileManager.default.createDirectory(atPath: dirPath.path, withIntermediateDirectories: true)
                let dbPath = dirPath.appendingPathComponent(Self.STORE_DB_NAME).path
                //Realizar conexion
                db = try Connection(dbPath)
                createTable()
                print("SQLite sucessfully created: \(dbPath)")
            }catch{
                print("Error: \(error)")
            }
        }
    }
    
    private func createTable(){
        guard let database = db else {
            return
        }
        do{
            // Creacion de la tabla
            try database.run(tasks.create {
                table in
                table.column(id, primaryKey: .autoincrement)
                table.column(taskName)
                table.column(date)
                table.column(status)
            })
            print("Table created...")
        }catch{
            print("error: \(error)")
        }
    }
    
    func getAllTasks() -> [Task] {
        var tasks: [Task] = []
        guard let database = db else {
            return []
        }
        do{
            // Consulta de los registros a la table
            for task in try database.prepare(self.tasks){
                tasks.append(Task(id: task[id], name: task[taskName], date: task[date], status: task[status]))
            }
                    
        }catch{
            print("error: \(error)")
        }
        return tasks
    }
    
    func insertTask(name: String, date: Date) -> Int64? {
        guard let database = db else {
            return nil
        }
        
        let insert = tasks.insert(
            self.taskName <- name,
            self.date <- date,
            self.status <- false
        )
        
        dump(insert)
        
        do {
            let rowId = try database.run(insert)
            return rowId
        }catch {
            print("Error: \(error)")
            return nil
        }
    }
    
    func runBatch(){
        guard let database =  db else {
            return
        }
        do {
            for index in 0...9{
                try database.run("INSERT INTO Tasks (name,date,status) VALUES (?,?,?)","Tarea \(index)","2023-06-24T17:39:00.000",0)
            }
        }catch{
            print(error)
        }
    }
    
    func completeStatement() throws {
        guard let database = db else {
            return
        }
        let stmt = try database.prepare("SELECT id, name from Tasks")
        for row in stmt {
            for (index, name) in stmt.columnNames.enumerated(){
                print("\(name):\(row[index]!)")
            }
        }
    }
    
}
