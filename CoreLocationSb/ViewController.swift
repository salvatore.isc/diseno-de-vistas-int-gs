//
//  ViewController.swift
//  CoreLocationSb
//
//  Created by Salvador Lopez on 21/06/23.
//

import UIKit

class ViewController: UIViewController {

    var statusLabel: UILabel!
    var wScreen = UIScreen.main.bounds.width
    var hScreen = UIScreen.main.bounds.height
    
    var locationService = LocationServices()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpGUI()
        initLocationServices()
    }

    func setUpGUI(){
        statusLabel = UILabel(frame: CGRect(x: (wScreen/2) - 200, y: hScreen/2, width: 400, height: 50))
        statusLabel.font = .systemFont(ofSize: 20)
        statusLabel.textAlignment = .center
        statusLabel.text = "Service Location..."
        statusLabel.numberOfLines = 0
        self.view.addSubview(statusLabel)
    }
    
    func initLocationServices(){
        locationService.delegate = self
        //validar si tengo permisos para el track location
        let isEnable = locationService.enable
        if isEnable {
            locationService.requestAuthorization()
        }else{
            //mostrar mensaje servicios de ubicacion
            locationServicesNeedState()
        }
    }

}

extension ViewController: LocationServicesDelegate {
    
    func authorizationRestricted() {
        locationServicesRestrictedState()
    }
    
    func authorizationUnknow() {
        locationServicesNeedState()
    }
    
    func promptAuthorizationAction() {
        let alert = UIAlertController(title: "Permisos de ubicacion", message: "Por favor acepte los permisos de ubicacion para brindar una mejor experiencia.", preferredStyle: .alert)
        let settingsAction = UIAlertAction(title: "Ir a configuracion", style: .default){
            _ in
            UIApplication.shared.open(URL(string: UIApplication.openSettingsURLString)!)
        }
        let cancelAction = UIAlertAction(title: "Cancelar", style: .cancel){
            _ in
            self.locationServicesNeedState()
        }
    }
    
    func didAuthorized() {
        locationService.start()
    }
    
}

extension ViewController {
    func locationServicesRestrictedState(){
        self.statusLabel.text = "El acceso a los servicios de ubicacion son necesarios para utilizar el app."
    }
    
    func locationServicesNeedState(){
        self.statusLabel.text = "Esta app esta restringida de los servicios de ubicacion."
    }
}

