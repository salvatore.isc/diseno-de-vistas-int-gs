//
//  ViewController.swift
//  MyStaticLibraryImplementation
//
//  Created by Salvador Lopez on 23/06/23.
//

import UIKit
import MyStaticLibrary


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        MyStaticLibrary.sayHello()
    }


}

